
package kg.attractor.microgram3.dto;

import kg.attractor.microgram3.model.Publication;
import lombok.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PublicationDTO {

    public static PublicationDTO from(Publication publication) {
        var publicationImageId = publication.getImage() == null
               ? "-no-image-id"
              : publication.getImage().getId();

        return builder()
                .id(publication.getId())
                .description(publication.getDescription())
                .publicationTime(publication.getPublicationTime())
                .imageId(publicationImageId)
                .build();
    }

    private String id = UUID.randomUUID().toString();
    private String description;
    private LocalDateTime publicationTime;
    private String imageId;
}
