package kg.attractor.microgram3.repository;

import kg.attractor.microgram3.model.Subscribes;
import org.springframework.data.repository.CrudRepository;

public interface SubscribesRepository extends CrudRepository<Subscribes, String> {
}
