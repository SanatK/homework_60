package kg.attractor.microgram3.repository;

import kg.attractor.microgram3.model.Like;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LikeRepository extends CrudRepository<Like, String> {
    List<Like> findAllByPublicationId(String publicationId);
}
