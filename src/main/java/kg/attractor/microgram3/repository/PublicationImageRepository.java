package kg.attractor.microgram3.repository;

import kg.attractor.microgram3.model.PublicationImage;
import org.springframework.data.repository.CrudRepository;

public interface PublicationImageRepository extends CrudRepository<PublicationImage, String> {
}
