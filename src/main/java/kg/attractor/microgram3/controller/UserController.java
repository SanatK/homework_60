package kg.attractor.microgram3.controller;

import kg.attractor.microgram3.dto.PublicationDTO;
import kg.attractor.microgram3.dto.UserDTO;
import kg.attractor.microgram3.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("*")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

   @PostMapping("/register")
   public UserDTO registerUser(@RequestBody UserDTO userData){
        return userService.addUser(userData);
   }
    @GetMapping("/profile/{userId}")
    public List<PublicationDTO> getProfile(@PathVariable String userId){
        return userService.getPublications(userId);
    }

}