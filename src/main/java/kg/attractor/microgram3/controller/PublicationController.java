package kg.attractor.microgram3.controller;

import kg.attractor.microgram3.dto.CommentDTO;
import kg.attractor.microgram3.dto.LikeDTO;
import kg.attractor.microgram3.dto.SubscribesDTO;
import kg.attractor.microgram3.model.Publication;
import kg.attractor.microgram3.model.User;
import kg.attractor.microgram3.repository.PublicationRepository;
import kg.attractor.microgram3.service.PublicationService;
import kg.attractor.microgram3.service.SubscribesService;
import kg.attractor.microgram3.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
public class PublicationController {
    private  PublicationService publicationService;
    private  UserService userService;
    private  SubscribesService subscriptionService;
    private  PublicationRepository publicationRepository;
    public PublicationController(PublicationService publicationService, UserService userService, SubscribesService subscriptionService, PublicationRepository publicationRepository){
        this.publicationService = publicationService;
        this.userService = userService;
        this.subscriptionService = subscriptionService;
        this.publicationRepository = publicationRepository;
    }
    @GetMapping
    public String root(Model model) {
        model.addAttribute("publications", epository.findAll());
        return "candidates";
    }

    @GetMapping("/publications")
    @ResponseBody
    public Iterable<Publication> getPublications(){
        return publicationRepository.findAll();
    }
    @PostMapping("/publications")
    public String rootSave(@RequestParam("text") String text,
                           @RequestParam("image") MultipartFile image) throws IOException {
       return publicationService.addPost(text, image);
    }
//    @PostMapping("/comment")
//    public String addComment(Authentication authentication, @RequestBody CommentDTO commentData){
//        User user = (User) authentication.getPrincipal();
//        return publicationService.addComment(commentData, user.getId());
//    }
//    @DeleteMapping("comment/{commentId}")
//    public ResponseEntity<Void> deleteComment(Authentication authentication, @PathVariable String commentId) {
//        User user = (User) authentication.getPrincipal();
//        if (publicationService.deleteComment(user.getId(), commentId))
//            return ResponseEntity.noContent().build();
//        return ResponseEntity.notFound().build();
//    }
//
//    @DeleteMapping("{userId}")
//    public ResponseEntity<Void> deleteUser(Authentication authentication) {
//        User user = (User) authentication.getPrincipal();
//        if (userService.deleteUser(user.getId()))
//            return ResponseEntity.noContent().build();
//        return ResponseEntity.notFound().build();
//    }
//
//    @DeleteMapping("/publication/{publicationId}")
//    public ResponseEntity<Void> deletePublication(Authentication authentication, @PathVariable String publicationId) {
//        User user = (User) authentication.getPrincipal();
//        if (publicationService.deletePublication(publicationId, user.getId()))
//            return ResponseEntity.noContent().build();
//        return ResponseEntity.notFound().build();
//    }
//    @PostMapping("/subscribe")
//    public SubscribesDTO subscribeOnUser(Authentication authentication, @RequestBody SubscribesDTO subscriptionData){
//        User user = (User) authentication.getPrincipal();
//        return subscriptionService.createSubscription(user.getId(), subscriptionData);
//    }
//    @PostMapping("/publication/like")
//    public String likePublication(Authentication authentication, @RequestBody LikeDTO likeData) {
//         User user = (User) authentication.getPrincipal();
//         return publicationService.likePublication(user.getId(), likeData);
//    }
}
