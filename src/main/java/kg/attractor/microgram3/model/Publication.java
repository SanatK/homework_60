package kg.attractor.microgram3.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Document(collection="publications")
public class Publication {
    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();
    private String description;
    private LocalDateTime publicationTime;
    private String userId;
    @DBRef
    private PublicationImage image;
}
