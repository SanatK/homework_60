package kg.attractor.microgram3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Microgram3Application {

    public static void main(String[] args) {
        SpringApplication.run(Microgram3Application.class, args);
    }

}
