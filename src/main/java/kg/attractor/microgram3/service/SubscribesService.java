package kg.attractor.microgram3.service;

import kg.attractor.microgram3.dto.SubscribesDTO;
import kg.attractor.microgram3.model.Subscribes;
import kg.attractor.microgram3.repository.SubscribesRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@AllArgsConstructor
@Service
public class SubscribesService {
    private final SubscribesRepository subscribesRepository;
    public SubscribesDTO createSubscription(String userId, SubscribesDTO subscriptionData){
        var subscription = Subscribes.builder()
                .id(subscriptionData.getId())
                .subscriberId(userId)
                .userId(subscriptionData.getUserId())
                .subscriptionTime(LocalDateTime.now())
                .build();
        subscribesRepository.save(subscription);
        return SubscribesDTO.from(subscription);
    }
}
